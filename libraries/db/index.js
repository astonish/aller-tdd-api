// Import dependencies
import Sqlite from 'sqlite'
import RandomString from 'randomstring'

// Import database setup script
import dbSetup from './setup'

// Import configuration for specified environment
const config = require(`../../environments/${process.env.NODE_ENV}`).default

// Connect to database
Sqlite.open(config.database.filename, { Promise }).then(() => {
  // Setup database structure
  dbSetup(Sqlite)
})

// Data access layer
class DB {
  constructor({ db }) {
    this.db = this.raw = db
  }

  // Reviews

  getReviews() {
    return this.db.all(`SELECT * FROM reviews`)
  }

  getReviewById(id) {
    return this.db.get(`SELECT * FROM reviews WHERE id = $id`, {
      $id: id
    })
  }

  createReview({ rating, content, userId }) {
    return this.db.run(`
      INSERT INTO reviews 
        (rating, content, userId)
      VALUES
        ($rating, $content, $userId);
    `, {
      $rating: rating,
      $content: content,
      $userId: userId,
    }).then(result => {
      return this.getReviewById(result.lastID)
    })
  }

  // Users

  getUserById(id) {
    return this.db.get(`SELECT * FROM users WHERE id = $id`, {
      $id: id
    })
  }

  ensureUser({ email }) {
    return this.db.run(`
      INSERT OR REPLACE INTO users 
        (email)
      VALUES
        ($email);
    `, {
      $email: email
    }).then(result => {
      return this.getUserById(result.lastID)
    })
  }

  // Auth token requests

  getAuthTokenRequestById(id) {
    return this.db.get(`SELECT * FROM auth_token_requests WHERE id = $id`, {
      $id: id
    })
  }

  getAuthTokenRequestBySecret(secret) {
    return this.db.get(`SELECT * FROM auth_token_requests WHERE secret = $secret`, {
      $secret: secret
    })
  }

  createAuthTokenRequest({ userId, secret, expires }) {
    return this.db.run(`
      INSERT INTO auth_token_requests 
        (expires, secret, userId)
      VALUES
        ($expires, $secret, $userId);
    `, {
      // Set expires or expire in two hours
      $expires: expires || Date.now() + 1000 * 60 * 60 * 2,

      // Set or generate secret
      $secret: secret || RandomString.generate({
        length: 128,
        charset: 'hex'
      }),

      // User id
      $userId: userId
    }).then(result => {
      return this.getAuthTokenRequestById(result.lastID)
    })
  }

  // Auth tokens

  getAuthTokenById(id) {
    return this.db.get(`SELECT * FROM auth_tokens WHERE id = $id`, {
      $id: id
    })
  }

  getAuthTokenByToken(token) {
    return this.db.get(`SELECT * FROM auth_tokens WHERE token = $token`, {
      $token: token
    })
  }

  createAuthToken({ userId, token, expires }) {
    return this.db.run(`
      INSERT INTO auth_tokens
        (expires, token, userId)
      VALUES
        ($expires, $token, $userId);
    `, {
      // Set expires or expire in 7 days
      $expires: expires || Date.now() + 1000 * 60 * 60 * 24 * 7,

      // Set token or generate one
      $token: token || RandomString.generate({
        length: 32,
        charset: 'hex'
      }),

      // User id
      $userId: userId
    }).then(result => {
      return this.getAuthTokenById(result.lastID)
    })
  }

  
}

// Export data acess layer
export default new DB({
  db: Sqlite
})