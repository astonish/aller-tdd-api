// Export setup method
export default function(db) {

  // Create users table
  db.run(`
    CREATE TABLE IF NOT EXISTS users (
      id INTEGER PRIMARY KEY AUTOINCREMENT,
      email TEXT
    );
  `)

  // Create reviews table
  db.run(`
    CREATE TABLE IF NOT EXISTS reviews (
      id INTEGER PRIMARY KEY AUTOINCREMENT,
      rating INTEGER,
      content TEXT,
      userId INTEGER,

      FOREIGN KEY(userId) REFERENCES users(id)
    );
  `)

  // Create auth token request table
  db.run(`
    CREATE TABLE IF NOT EXISTS auth_token_requests (
      id INTEGER PRIMARY KEY AUTOINCREMENT,
      expires INTEGER,
      secret TEXT,
      userId INTEGER,

      FOREIGN KEY(userId) REFERENCES users(id)
    );
  `)
  
  // Create auth token request table
  db.run(`
    CREATE TABLE IF NOT EXISTS auth_tokens (
      id INTEGER PRIMARY KEY AUTOINCREMENT,
      expires INTEGER,
      token TEXT,
      userId INTEGER,

      FOREIGN KEY(userId) REFERENCES users(id)
    );
  `)
}