// Mail layer
class Mail{
  constructor({ debug }) {
    this.debug = debug
  }

  dispatchAuthTokenRequestEmail({ email, secret }) {
    // We are faking sending an email. This is where
    // you would integrate towards a third party email or the like
    if (this.debug) console.log(`>> Pretending to send email to ${email}`)

    // Fantastic security error
    if (this.debug) console.log(`>> Super secret: ${secret}`)
  }
}

// Export mail layer
export default new Mail({
  debug: process.env.NODE_ENV === 'development'
})