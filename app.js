// Import dependencies
import Express from 'express'
import CookieParser from 'cookie-parser'
import BodyParser from 'body-parser'
import Boom from 'boom'

// Import routes
import routes from './routes'

// Import db and mail libraries
import db from './libraries/db'
import mail from './libraries/mail'

// Instantiate app
const app = Express()

// Make db and mail libraries available to app
app.set('db', db)
app.set('mail', mail)

// Initialize middleware
app.use(BodyParser.json())
app.use(BodyParser.urlencoded({ extended: true }))
app.use(CookieParser())

// Inject libraries to requests
app.use((req, res, next) => {
  // Attach db and mail library references to request
  req.db = app.get('db')
  req.mail = app.get('mail')

  // Proceed
  next()
})

// Attach api routes
app.use('/api', routes)

// Catch and send 404 to error handler
app.use((req, res, next) => {
  // Forward generic 404 error
  next(Boom.notFound())
})

// Error handler
app.use((err, req, res, next) => {
  
  // Infer that it is a joi validation error
  if (err.errors) {
    // Wrap joi error in boom
    err = Boom.create(
      err.status, 
      err.errors.map(error => error.messages.join(', '))
    )
  }
  
  // Check if error is already wrapped
  if (!err.isBoom) {
    // If not, wrap it in http-friendly error
    err = Boom.wrap(err)
  }

  // Check if we are in development environment
  if (['development'].indexOf(req.app.get('env')) !== -1) {
    // Log error to console
    console.log(err)
  }

  // Set status code
  res.status(err.output.statusCode)

  // Send error payload
  res.json(err.output.payload)
})

// Export app
export default app