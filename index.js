// Import application
import app from './app'

// Import configuration for specified environment
const config = require(`./environments/${process.env.NODE_ENV}`).default

// Start server
app.listen(config.server.port, config.server.host, config.server.backlog, () => {
  console.log(`Server running on ${config.server.host}:${config.server.port}`)
})