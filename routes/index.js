// Import dependencies
import Express from 'express'

// Import routes
import status from './status'
import auth from './auth'
import reviews from './reviews'

// Instantiate express router
const router = Express.Router()

// Setup routes
router.use('/status', status)
router.use('/auth', auth)
router.use('/reviews', reviews)

// Export router
export default router