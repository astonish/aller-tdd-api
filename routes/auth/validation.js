// Import dependencies
import Joi from 'joi'

// Export validation schema
export default {
  requestToken: {
    body: {
      email: Joi.string().email().required()
    }
  },

  exchangeToken: {
    body: {
      secret: Joi.string().hex().length(128).required()
    }
  }
}