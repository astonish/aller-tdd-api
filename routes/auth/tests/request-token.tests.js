// Import dependencies
import Supertest from 'supertest'
import Should from 'should'

// Import server
import app from '../../../app'

// Mail stub
class MailStub {
  constructor() {
    this.latestEmailOptions = {}
  }

  // Mimicking real method
  dispatchAuthTokenRequestEmail({ email, secret }) {
    this.latestEmailOptions = {
      email: email,
      mergeVars: {
        secret: secret
      }
    }
  }

  // Extra method for retrieving state
  getLatestEmailOptions() {
    return this.latestEmailOptions
  }
}

// Override existing library with mock
app.set('mail', new MailStub())

// Wrap server in supertest
const server = Supertest.agent(app)

// Api reviews
describe('API /api/auth/request-token', () => {

  // Setup before each test handler
  beforeEach(() => {
    // Remove all auth token request records
    return Promise.all([
      app.get('db').raw.run(`DELETE FROM users`),
      app.get('db').raw.run(`DELETE FROM auth_token_requests`)
    ])
  })

  // Request auth secret
  describe('POST /', () => {

    // Missing email

    it('should return a 400 bad request error if email is empty', () => {
      // Arrange
      const payload = {
        email: ''
      }

      // Act
      const request = server
        .post('/api/auth/request-token')
        .send(payload)

      // Assert
      return request
        .expect('Content-type', /json/)
        .expect(400)
        .then(response => {
          response.body.should.have.property('statusCode', 400)
          response.body.should.have.property('error', 'Bad Request')
          response.body.should.have.property('message', '"email" is not allowed to be empty, "email" must be a valid email')
        })
    })

    // Invalid email format

    it('should return a 400 bad request error if email has an invalid format', () => {
      // Arrange
      const payload = {
        email: 'bad@.dk'
      }

      // Act
      const request = server
        .post('/api/auth/request-token')
        .send(payload)

      // Assert
      return request
        .expect('Content-type', /json/)
        .expect(400)
        .then(response => {
          response.body.should.have.property('statusCode', 400)
          response.body.should.have.property('error', 'Bad Request')
          response.body.should.have.property('message', '"email" must be a valid email')
        })
    })

    // Create auth token request

    it('should create an auth token request given a valid email address', () => {
      // Arrange
      const db = app.get('db').raw

      const payload = {
        email: 'henrik@astonish.dk'
      }

      // Act
      const request = server
        .post('/api/auth/request-token')
        .send(payload)

      // Assert
      return request
        .expect('Content-type', /json/)
        .expect(200)
        .then(response => {
          response.body.should.be.empty

          // Assert user state in db
          return db.get(`SELECT id, email FROM users WHERE email = $email`, {
            $email: payload.email
          }).then(user => {

            // Assert state
            Should.exist(user)
            user.should.have.property('email', payload.email)

            // Assert auth token state in db
            return db.get(`SELECT id, secret FROM auth_token_requests WHERE userId = $userId`, {
              $userId: user.id
            }).then(authTokenRequest => {

              // Assert state
              Should.exist(authTokenRequest)
              authTokenRequest.secret.should.not.be.empty
            })
          })
        })
    })

    // Dispatch an email with auth secret

    it('should dispatch an email with auth secret given a valid email address', () => {
      // Arrange
      const mailStub = app.get('mail')

      const payload = {
        email: 'henrik@astonish.dk'
      }

      // Act
      const request = server
        .post('/api/auth/request-token')
        .send(payload)

      // Assert
      request
        .expect('Content-type', /json/)
        .expect(200)
        .then(response => {
          response.body.should.be.empty

          const emailOptions = mailStub.getLatestEmailOptions()

          emailOptions.email.should.equal(payload.email)
          emailOptions.mergeVars.secret.should.not.be.empty
        })
    })
  })
})