// Import dependencies
import Supertest from 'supertest'
import Should from 'should'

// Import server
import app from '../../../app'

// Wrap server in supertest
const server = Supertest.agent(app)

// Helper method for preparing auth token request db state
const prepareAuthTokenRequest = (params) => {
  // Get db reference
  const db = app.get('db')

  // Create user
  return db.ensureUser({ 
    email: params.user.email 
  }).then(user => {

    // Create auth token request
    return db.createAuthTokenRequest({ 
      secret: params.authTokenRequest.secret, 
      expires: params.authTokenRequest.expires, 
      userId: user.id 
    }).then(authTokenRequest => {
      return authTokenRequest
    })
  })
}

// Api reviews
describe('API /api/auth/exchange-token', () => {

  // Setup before each test handler
  beforeEach(() => {
    // Remove all auth token request and and auth token rows
    return Promise.all([
      app.get('db').raw.run(`DELETE FROM users`),
      app.get('db').raw.run(`DELETE FROM auth_token_requests`),
      app.get('db').raw.run(`DELETE FROM auth_tokens`),
    ])
  })

  // Request auth token
  describe('POST /', () => {

    // Missing secret

    it('should return a 400 bad request error if secret is empty', () => {
      // Arrange
      const payload = {
        secret: ''
      }

      // Act
      const request = server
        .post('/api/auth/exchange-token')
        .send(payload)

      // Assert
      return request
        .expect('Content-type', /json/)
        .expect(400)
        .then(response => {
          response.body.should.have.property('statusCode', 400)
          response.body.should.have.property('error', 'Bad Request')
          response.body.should.have.property('message', '"secret" is not allowed to be empty, "secret" must only contain hexadecimal characters, "secret" length must be 128 characters long')
        })
    })

    // Invalid secret format

    it('should return a 400 bad request error if secret has an invalid format', () => {
      // Arrange
      const payload = {
        secret: '_badSecretFormat?'
      }

      // Act
      const request = server
        .post('/api/auth/exchange-token')
        .send(payload)

      // Assert
      return request
        .expect('Content-type', /json/)
        .expect(400)
        .then(response => {
          response.body.should.have.property('statusCode', 400)
          response.body.should.have.property('error', 'Bad Request')
          response.body.should.have.property('message', '"secret" must only contain hexadecimal characters, "secret" length must be 128 characters long')
        })
    })

    // Valid secret, but no auth token request

    it('should return a 400 bad request error if no auth token request is associated with secret', () => {
      // Arrange
      const payload = {
        secret: '394181037e4cd3baa762cecbc91cbcd1b9b37b75177d717abfa305efe7652611e6d2ecfb96ff494981127e3bdb516ba5fdcc7e3424e4243a1658f4be09d27f36'
      }

      // Act
      const request = server
        .post('/api/auth/exchange-token')
        .send(payload)

      // Assert
      return request
        .expect('Content-type', /json/)
        .expect(404)
        .then(response => {
          response.body.should.have.property('statusCode', 404)
          response.body.should.have.property('error', 'Not Found')
          response.body.should.have.property('message', 'Auth token request not found')
        })
    })

    // Valid, but expired secret

    it('should return a 400 bad request error if secret has expired', () => {
      // Arrange
      const dbPrepare = prepareAuthTokenRequest({
        user: {
          email: 'henrik@astonish.dk',
        },
        authTokenRequest: {
          secret: 'da1954dcc4a99ad780fef1f2ce5bd4f2396911a175f5d6845ea818cd1b795045c57f52406c10400e0f55e60a5ae9a5363ee1123e3df3e76e37731dcb33424a82',
          expires: Date.now() - 10000
        }
      })

      return dbPrepare.then(authTokenRequest => {
        const payload = {
          secret: authTokenRequest.secret
        }

        // Act
        const request = server
          .post('/api/auth/exchange-token')
          .send(payload)

        // Assert
        return request
          .expect('Content-type', /json/)
          .expect(400)
          .then(response => {
            response.body.should.have.property('statusCode', 400)
            response.body.should.have.property('error', 'Bad Request')
            response.body.should.have.property('message', 'Auth token request has expired')
          })
      })
    })

    // Valid and existing secret, create auth token

    it('should create and return an auth token given a valid secret', () => {
      // Arrange
      const db = app.get('db').raw

      const dbPrepare = prepareAuthTokenRequest({
        user: {
          email: 'henrik@astonish.dk',
        },
        authTokenRequest: {
          secret: 'da1954dcc4a99ad780fef1f2ce5bd4f2396911a175f5d6845ea818cd1b795045c57f52406c10400e0f55e60a5ae9a5363ee1123e3df3e76e37731dcb33424a82'
        }
      })

      return dbPrepare.then(authTokenRequest => {
        const payload = {
          secret: authTokenRequest.secret
        }

        // Act
        const request = server
          .post('/api/auth/exchange-token')
          .send(payload)

        // Assert
        return request
          .expect('Content-type', /json/)
          .expect(200)
          .then(response => {
            response.body.token.should.not.be.empty

            // Assert db state
            return db.get(`SELECT id, token, expires, userId FROM auth_tokens WHERE token = $token`, {
              $token: response.body.token
            }).then(authToken => {
              // Assert state
              Should.exists(authToken)
              authToken.token.should.not.be.empty
              authToken.userId.should.not.be.empty
              authToken.expires.should.be.above(Date.now() + 1000 * 60 * 60 * 24 * 7 - 10000)
            })
          })
      })
    })
  })
})