// Import dependencies
import Boom from 'boom'

// Request auth token method
function requestToken(req, res, next) {
  // Get db and mail references
  const db = req.db
  const mail = req.mail

  // Upsert user in db
  db.ensureUser({
    email: req.body.email
  }).then(user => {

    // Create auth token request
    db.createAuthTokenRequest({
      userId: user.id
    }).then(authTokenRequest => {

      // Dispatch email to user
      mail.dispatchAuthTokenRequestEmail({
        email: user.email,
        secret: authTokenRequest.secret
      })
      
      // Send empty response
      res.json({})
    })
  })
}

// Exchange auth secret to auth token method
function exchangeToken(req, res, next) {
  // Get db reference
  const db = req.db

  // Get auth token request
  db.getAuthTokenRequestBySecret(req.body.secret).then(authTokenRequest => {

    // Check if auth token exists
    if (!authTokenRequest) {
      // Proceed with not found error
      return next(Boom.notFound('Auth token request not found'))
    }

    // Check if auth token request has expired
    if (authTokenRequest.expires < Date.now()) {
      // Proceed with error if secret has expired
      return next(Boom.badRequest('Auth token request has expired'))
    }

    // Create auth token
    db.createAuthToken({
      userId: authTokenRequest.userId
    }).then(authToken => {
      
      // Send auth token as response
      res.json({
        token: authToken.token
      })
    })
  })
}

// Export methods
export default {
  requestToken,
  exchangeToken
}
