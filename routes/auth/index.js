// Import dependencies
import Express from 'express'
import Validate from 'express-validation'

// Get validation schema
import validation from './validation'

// Get controller
import controller from './controller'

// Instantiate express router
const router = Express.Router()

// Attach auth token request route
router
  .route('/request-token')
  .post(
    Validate(validation.requestToken),
    controller.requestToken
  )

// Attach auth token exchange route
router
  .route('/exchange-token')
  .post(
    Validate(validation.exchangeToken),
    controller.exchangeToken
  )

// Export router
export default router