// Import dependencies
import Joi from 'joi'

// Export validation schema
export default {
  findOne: {
    params: {
      reviewId: Joi.number().integer().min(0).required()
    }
  },

  create: {
    body: {
      rating: Joi.number().integer().min(1).max(5).required(),
      content: Joi.string().required()
    }
  }
}