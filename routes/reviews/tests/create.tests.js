// Import dependencies
import Supertest from 'supertest'
import Should from 'should'

// Import server
import app from '../../../app'

// Wrap server in supertest
const server = Supertest.agent(app)

// Helper
const getValidAuthToken = (params) => {
  // Get db reference
  const db = app.get('db')

  // Ensure user
  return db.ensureUser({ 
    email: params.user.email
  }).then(user => {
    // Create auth token
    return db.createAuthToken({ 
      userId: user.id,
      ...params.authToken
    }).then(authToken => ({
      user,
      authToken
    }))
  })
}

// Create a post
describe('POST /api/reviews', () => {

  // Setup before each test handler

  beforeEach(() => {
    // Remove all review rows
    return Promise.all([
      app.get('db').raw.run(`DELETE FROM users`),
      app.get('db').raw.run(`DELETE FROM reviews`)
    ])
  })

  // Missing credentials

  it('should return a 401 unauthorized error if auth token is missing', () => {
    // Arrange
    const payload = {
      rating: 3,
      content: 'Average'
    }

    const headers = {}

    // Act
    const request = server
      .post(`/api/reviews`)
      .set(headers)
      .send(payload)

    // Assert
    return request
      .expect('Content-type', /json/)
      .expect(401)
      .then(response => {
        response.body.should.have.property('statusCode', 401)
        response.body.should.have.property('error', 'Unauthorized')
        response.body.should.have.property('message', 'Missing auth token')
      })
  })

  // Invalid credentials

  it('should return a 401 unauthorized error if auth token is invalid', () => {
    // Arrange
    const payload = {
      rating: 3,
      content: 'Average'
    }

    const headers = {
      'Authorization': `Bearer wrongCredentials`
    }

    // Act
    const request = server
      .post(`/api/reviews`)
      .set(headers)
      .send(payload)

    // Assert
    return request
      .expect('Content-type', /json/)
      .expect(401)
      .then(response => {
        response.body.should.have.property('statusCode', 401)
        response.body.should.have.property('error', 'Unauthorized')
        response.body.should.have.property('message', 'Invalid auth token')
      })
  })

  // Expired credentials

  it('should return a 401 unauthorized error if auth token is expired', () => {
    // Arrange
    const authTokenParams = {
      user: {
        email: 'henrik@astonish.dk'
      },
      authToken: {
        expires: Date.now() - 10000
      }
    }

    return getValidAuthToken(authTokenParams).then(({ authToken }) => {
      const payload = {
        rating: 3,
        content: 'Average'
      }

      const headers = {
        'Authorization': `Bearer ${authToken.token}`
      }

      // Act
      const request = server
        .post(`/api/reviews`)
        .set(headers)
        .send(payload)

      // Assert
      return request
        .expect('Content-type', /json/)
        .expect(401)
        .then(response => {
          response.body.should.have.property('statusCode', 401)
          response.body.should.have.property('error', 'Unauthorized')
          response.body.should.have.property('message', 'Expired auth token')
        })
    })
  })

  // Bad payload
  
  it('should return a 400 bad request error if given a bad payload', () => {
    // Arrange
    const authTokenParams = {
      user: {
        email: 'henrik@astonish.dk'
      }
    }

    return getValidAuthToken(authTokenParams).then(({ authToken }) => {
      const payload = {
        rating: -1,
        content: null
      }

      const headers = {
        'Authorization': `Bearer ${authToken.token}`
      }

      // Act
      const request = server
        .post(`/api/reviews`)
        .set(headers)
        .send(payload)

      // Assert
      return request
        .expect('Content-type', /json/)
        .expect(400)
        .then(response => {
          response.body.should.have.property('statusCode', 400)
          response.body.should.have.property('error', 'Bad Request')
          response.body.should.have.property('message', '"rating" must be larger than or equal to 1,"content" must be a string')
        })
    })
  })

  // Create review
  
  it('should create and return a review given a valid payload and auth token', () => {
    // Arrange
    const authTokenParams = {
      user: {
        email: 'henrik@astonish.dk'
      }
    }

    return getValidAuthToken(authTokenParams).then(({ authToken, user }) => {
      const payload = {
        rating: 5,
        content: 'Awesome'
      }

      const headers = {
        'Authorization': `Bearer ${authToken.token}`
      }

      // Act
      const request = server
        .post(`/api/reviews`)
        .set(headers)
        .send(payload)

      // Assert
      return request
        .expect('Content-type', /json/)
        .expect(200)
        .then(response => {
          response.body.should.have.property('review')
          response.body.review.should.have.property('id')
          response.body.review.should.have.property('rating', payload.rating)
          response.body.review.should.have.property('content', payload.content)
          response.body.review.should.have.property('userId', user.id)

          // State db check
          return app.get('db').raw.get(`SELECT id, rating, content, userId FROM reviews WHERE id = $id`, {
            $id: response.body.review.id
          }).then(review => {
            // Assert state
            Should.exist(review)
            review.should.have.property('rating', payload.rating)
            review.should.have.property('content', payload.content)
            review.should.have.property('userId', user.id)
          })
        })
    })
  })
})