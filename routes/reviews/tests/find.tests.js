// Import dependencies
import Supertest from 'supertest'
import Should from 'should'

// Import server
import app from '../../../app'

// Wrap server in supertest
const server = Supertest.agent(app)

// Get reviews
describe('GET /api/reviews', () => {

  // Setup before each test handler
  
  beforeEach(() => {
    // Remove all review rows
    return app.get('db').raw.run(`DELETE FROM reviews`)
  })

  // List without reviews

  it('should return an empty list of reviews', () => {
    // Arrange

    // Act
    const request = server
      .get('/api/reviews')

    // Assert
    return request
      .expect('Content-type', /json/)
      .expect(200)
      .then(response => {
        response.body
          .should.have.property('reviews')
          .and.be.an.instanceOf(Array)
          .and.have.lengthOf(0)
          
      })
  })

  // List with 3 review entries

  it('should return a list of all reviews', () => {
    // Arrange
    const dbPrepare = app.get('db').raw.run(`
      INSERT INTO reviews 
        (rating, content) 
      VALUES
        (3, "It was alright"),
        (1, "It was poor"),
        (5, "It was awesome");
    `)
    
    return dbPrepare.then(() => {
      
      // Act
      const request = server
        .get('/api/reviews')

      // Assert
      return request
        .expect('Content-type', /json/)
        .expect(200)
        .then(response => {
          response.body
            .should.have.property('reviews')
          
          response.body.reviews
            .should.be.an.instanceOf(Array)
            .and.have.lengthOf(3)
        })
      })
    })
})