// Import dependencies
import Supertest from 'supertest'
import Should from 'should'

// Import server
import app from '../../../app'

// Wrap server in supertest
const server = Supertest.agent(app)

// Get review by id
describe('GET /api/reviews/:reviewId', () => {

  // Setup before each test handler

  beforeEach(() => {
    // Remove all review rows
    return app.get('db').raw.run(`DELETE FROM reviews`)
  })

  // Review does not exist
  
  it('should return a 404 error if review does not exist', () => {
    // Arrange
    const reviewId = 42

    // Act
    const request = server
      .get(`/api/reviews/${reviewId}`)

    // Assert
    return request
      .expect('Content-type', /json/)
      .expect(404)
      .then(response => {
        response.body.should.have.property('statusCode', 404)
        response.body.should.have.property('error', 'Not Found')
      })
  })

  // Review queried using malformed id

  it('should return 400 bad request error if given a malformed id', () => {
    // Arrange
    const reviewId = "i-am-ill-formatted"

    // Act
    const request = server
      .get(`/api/reviews/${reviewId}`)

    // Assert
    return request
      .expect('Content-type', /json/)
      .expect(400)
      .then(response => {
        response.body.should.have.property('statusCode', 400)
        response.body.should.have.property('error', 'Bad Request')
        response.body.should.have.property('message', '"reviewId" must be a number')
      })
  })

  // Review queried using negative integer as id

  it('should return 400 bad request error if given a negative integer as id', () => {
    // Arrange
    const reviewId = -1

    // Act
    const request = server
      .get(`/api/reviews/${reviewId}`)

    // Assert
    return request
      .expect('Content-type', /json/)
      .expect(400)
      .then(response => {
        response.body.should.have.property('statusCode', 400)
        response.body.should.have.property('error', 'Bad Request')
        response.body.should.have.property('message', '"reviewId" must be larger than or equal to 0')
      })
  })

  // Review queried using float as id

  it('should return 400 bad request error if given a float as id', () => {
    // Arrange
    const reviewId = 1.2

    // Act
    const request = server
      .get(`/api/reviews/${reviewId}`)

    // Assert
    return request
      .expect('Content-type', /json/)
      .expect(400)
      .then(response => {
        response.body.should.have.property('statusCode', 400)
        response.body.should.have.property('error', 'Bad Request')
        response.body.should.have.property('message', '"reviewId" must be an integer')
      })
  })

  // Review given an existing id

  it('should return a review given an id', () => {
    // Arrange
    const review = {
      rating: 5,
      content: "Best thing ever"
    }

    const dbPrepare = app.get('db').raw.run(`
      INSERT INTO reviews 
        (rating, content) 
      VALUES
        (${review.rating}, "${review.content}");
    `)
    
    return dbPrepare.then((result) => {
      const reviewId = result.lastID // Get id from database query
      
      // Act
      const request = server
        .get(`/api/reviews/${reviewId}`)

      // Assert
      return request
        .expect('Content-type', /json/)
        .expect(200)
        .then(response => {
          response.body.should.have.property('review')
          response.body.review.should.have.property('id', reviewId)
          response.body.review.should.have.property('rating', review.rating)
          response.body.review.should.have.property('content', review.content)
        })
    })
  })
})