// Import dependencies
import Boom from 'boom'

// Get reviews method
function find(req, res, next) {
  // Get db reference
  const db = req.db

  // Query db for all reviews
  db.getReviews().then(reviews => {
    // Send reviews as response
    res.json({ reviews })
  })
}

// Get specific review
function findOne(req, res, next) {
  // Get db reference
  const db = req.db

  // Query db for review
  db.getReviewById(req.params.reviewId).then(review => {

    // If review is not found
    if (!review) {
      // Proceed with not found error
      return next(Boom.notFound())
    }

    // Send review as response
    res.json({ review })
  })
}

// Create review
function create(req, res, next) {
  // Get db reference
  const db = req.db

  // Get authorization
  const authorization = req.headers.Authorization || req.headers.authorization || ''

  // Extract token
  const token = authorization.replace('Bearer ', '')

  // No token exists
  if (!token) {
    // Return missing auth token error
    return next(Boom.unauthorized('Missing auth token'))
  }

  // Get auth token
  db.getAuthTokenByToken(token).then(authToken => {
    // No token was found
    if (!authToken) {
      // Infer invalid auth token
      return next(Boom.unauthorized('Invalid auth token'))
    }

    // Check if token is expired
    if (authToken.expires < Date.now()) {
      return next(Boom.unauthorized('Expired auth token'))
    }

    // Fake response
    // res.json({
    //   review: {
    //     id: 1337,
    //     rating: req.body.rating,
    //     content: req.body.content,
    //     userId: authToken.userId
    //   }
    // })

    // Prepare review properties
    const properties = {
      rating: req.body.rating,
      content: req.body.content,
      userId: authToken.userId
    }

    // Create review
    db.createReview(properties).then(review => {
      // Send review as response
      res.json({ review })
    })

  })
}

// Export methods
export default {
  find,
  findOne,
  create
}
