// Import dependencies
import Express from 'express'
import Validate from 'express-validation'

// Get validation schema
import validation from './validation'

// Get controller
import controller from './controller'

// Instantiate express router
const router = Express.Router()

// Attach get all route
router
  .route('/')
  .get(controller.find)

// Attach get one route
router
  .route('/:reviewId')
  .get(
    Validate(validation.findOne),
    controller.findOne
  )

// Attach create route
router
  .route('/')
  .post(
    Validate(validation.create),
    controller.create
  )

// Export router
export default router