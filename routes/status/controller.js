// Status method
function status(req, res, next) {
  // Report server status
  res.json({
    health: 'OK'
  })
}

// Export methods
export default {
  status
}