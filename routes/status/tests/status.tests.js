// Import dependencies
import Supertest from 'supertest'
import Should from 'should'

// Import server
import app from '../../../app'

// Wrap server in supertest
const server = Supertest.agent(app)

// Api status
describe('GET /api/status', () => {
  
  it('should return server status', () => {
    // Arrange

    // Act
    const request = server
      .get('/api/status')

    // Assert
    return request
      .expect('Content-type', /json/)
      .expect(200)
      .then(response => {
        response.body.health.should.equal('OK')
      })
  })
})