// Import dependencies
import Express from 'express'

// Get controllers
import controller from './controller'

// Instantiate express router
const router = Express.Router()

// Attach routes
router.get('/', controller.status)

// Export router
export default router