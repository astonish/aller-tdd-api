// Import common configuration
import common from './common'

// Make a copy of common configuration
const config = Object.assign({}, common)

// Replace database authentication information
config.database.filename = ':memory:'

// Export configuration
export default config