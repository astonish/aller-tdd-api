// Export configuration common for all environments
export default {
  server: {
    port: process.env.PORT || 3000,
    host: process.env.HOST || '0.0.0.0',
    backlog: 511
  },

  database: {
    filename: null
  }
}