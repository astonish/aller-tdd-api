### Server

#### Installation

To install server dependencies execute the following command

    npm install

#### Running

To run the server execute the following command

    NODE_ENV={environment} npm start

#### Test

To run the test suite execute the following command

    npm test
    - or-
    npm run test-coverage


### Routes

#### Status

| Method | Route | Auth | Params | Response | Errors | Description | 
| ------ | ----- | ---- | ------ | -------- | ------ | ----------- |
| GET | /api/status | | | health: String | | Returns the health status of the server |

#### Authentication

| Method | Route | Auth | Params | Response | Errors | Description | 
| ------ | ----- | ---- | ------ | -------- | ------ | ----------- |
| POST | /api/auth/request-token | | email: String | | Invalid email | Sends an auth secret to the specified email |
| POST | /api/auth/exchange-token | | secret: String | token: String | Invalid token<br>Expired token | Converts an auth secret to an auth token |

#### Reviews

| Method | Route | Auth | Params | Response | Errors | Description | 
| ------ | ----- | ---- | ------ | -------- | ------ | ----------- |
| GET | /api/reviews | | | reviews: [Review] |  | Returns all reviews. |
| GET | /api/reviews/:reviewId | | | review: Review | Not found  | Returns specific review by id. |
| POST | /api/reviews | Required | rating: Number<br />content: String | review: Review | Invalid rating<br />Invalid content<br />Not authenticated | Creates a new review. User is identified through auth token in headers. |

### Types

#### User

    {
        id: Id,
        email: String
    }

#### Review

    {
        id: Id,
        rating: Number,
        content: String,
        author: User
    }